package com.codefirst.simonsaysfirst.gameactivity.di

import com.codefirst.simonsaysfirst.gameactivity.logic.GameMode
import com.codefirst.simonsaysfirst.gameactivity.presentation.GameActivity
import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent

@Subcomponent(modules = [GameActivityModule::class,
    GameLogicInformationModule::class])
interface GameActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun buildGameMode(gameMode: GameMode): Builder

        fun build(): GameActivityComponent
    }

    fun inject(activity: GameActivity)
}