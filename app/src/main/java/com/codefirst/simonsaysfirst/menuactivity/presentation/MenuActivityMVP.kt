package com.codefirst.simonsaysfirst.menuactivity.presentation

interface MenuActivityMVP {

    interface Presenter {

        fun attach(view: View)
        fun detach()
        fun openInfiniteMode()
        fun openStandardMode()
        fun openAboutApp()
    }

    interface View {

    }
}