package com.codefirst.simonsaysfirst.navigation

interface AndroidNavigation {

    fun openInfiniteMode()
    fun openStandardMode()
}