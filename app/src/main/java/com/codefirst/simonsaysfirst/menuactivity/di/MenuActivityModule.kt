package com.codefirst.simonsaysfirst.menuactivity.di

import com.codefirst.simonsaysfirst.menuactivity.presentation.MenuActivityMVP
import com.codefirst.simonsaysfirst.menuactivity.presentation.MenuActivityPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class MenuActivityModule {

    @Binds
    abstract fun bindPresenter(presenter: MenuActivityPresenter): MenuActivityMVP.Presenter
}