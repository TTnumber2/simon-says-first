package com.codefirst.simonsaysfirst.menuactivity.presentation

import com.codefirst.simonsaysfirst.navigation.AndroidNavigation
import javax.inject.Inject

class MenuActivityPresenter
@Inject constructor(private val androidNavigation: AndroidNavigation) : MenuActivityMVP.Presenter {

    private var view: MenuActivityMVP.View? = null

    override fun attach(view: MenuActivityMVP.View) {
        this.view = view
    }

    override fun openInfiniteMode() {
        androidNavigation.openInfiniteMode()
    }

    override fun openStandardMode() {
        androidNavigation.openStandardMode()
    }

    override fun openAboutApp() {
        //NONE for now
    }

    override fun detach() {
        view = null
    }
}