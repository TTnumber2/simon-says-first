package com.codefirst.simonsaysfirst.navigation

import android.content.Context
import com.codefirst.simonsaysfirst.gameactivity.presentation.GameActivity.Companion.openGameWithInfiniteMode
import com.codefirst.simonsaysfirst.gameactivity.presentation.GameActivity.Companion.openGameWithStandardMode
import javax.inject.Inject

class AndroidNavigationImpl
@Inject constructor(private val context: Context) : AndroidNavigation {

    override fun openInfiniteMode() {
        openGameWithInfiniteMode(context)
    }

    override fun openStandardMode() {
        openGameWithStandardMode(context)
    }
}