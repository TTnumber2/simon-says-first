package com.codefirst.simonsaysfirst.application

import android.app.Activity
import android.content.Context
import javax.inject.Inject

class SharedPrefsImpl
@Inject constructor(context: Context) : SharedPrefs {

    companion object {
        const val PREFERENCES_NAME = "simon_says_first_prefs"
        const val BEST_SCORE_INFINITE = "best_score_infinite"
    }

    private val sharedPrefs = context.getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE)

    override fun saveBestScoreInfinityGameMode(best: Int) {
        sharedPrefs.edit().putInt(BEST_SCORE_INFINITE, best).commit()
    }

    override fun getBestScoreInfinityGameMode(): Int = sharedPrefs.getInt(BEST_SCORE_INFINITE, 0)
}