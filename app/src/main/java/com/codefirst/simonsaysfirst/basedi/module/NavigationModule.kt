package com.codefirst.simonsaysfirst.basedi.module

import com.codefirst.simonsaysfirst.application.SharedPrefs
import com.codefirst.simonsaysfirst.application.SharedPrefsImpl
import com.codefirst.simonsaysfirst.navigation.AndroidNavigation
import com.codefirst.simonsaysfirst.navigation.AndroidNavigationImpl
import dagger.Binds
import dagger.Module

@Module
abstract class NavigationModule {

    @Binds
    abstract fun bindNagivator(navigator: AndroidNavigationImpl): AndroidNavigation

    @Binds
    abstract fun bindSharedPrefs(prefs: SharedPrefsImpl): SharedPrefs
}