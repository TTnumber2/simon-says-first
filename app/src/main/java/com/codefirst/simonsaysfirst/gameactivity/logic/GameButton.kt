package com.codefirst.simonsaysfirst.gameactivity.logic

enum class GameButton {

    GREEN,
    RED,
    YELLOW,
    BLUE
}