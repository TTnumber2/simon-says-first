package com.codefirst.simonsaysfirst.gameactivity.di

import com.codefirst.simonsaysfirst.gameactivity.logic.GameLogic
import com.codefirst.simonsaysfirst.gameactivity.logic.GameLogicImpl
import com.codefirst.simonsaysfirst.gameactivity.presentation.GameActivityMVP
import com.codefirst.simonsaysfirst.gameactivity.presentation.GameActivityPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class GameActivityModule {

    @Binds
    abstract fun bindGameLogic(logic: GameLogicImpl): GameLogic

    @Binds
    abstract fun bindPresenter(presenter: GameActivityPresenter): GameActivityMVP.Presenter
}