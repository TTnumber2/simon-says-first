package com.codefirst.simonsaysfirst.gameactivity.di

import com.codefirst.simonsaysfirst.gameactivity.logic.GameInformation
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

@Module
class GameLogicInformationModule {

    private val gameInformationSubject: Subject<GameInformation> = BehaviorSubject.create()

    @Provides
    fun updateTasksStatusObserver(): Observer<GameInformation> = gameInformationSubject

    @Provides
    fun updateTasksStatusObservable(): Observable<GameInformation> = gameInformationSubject.hide()
}