package com.codefirst.simonsaysfirst.rxextension

import com.codefirst.simonsaysfirst.basedi.module.SchedulersModule.SchedulerUI
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RxExtensionSchedulerImpl
@Inject constructor(@SchedulerUI private val uiScheduler: Scheduler) : RxExtensionScheduler {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun scheduleInterval(): Observable<Long> =
            Observable.create { emitter ->
                Observable.intervalRange(0L, 2L, 0L, 1L, TimeUnit.SECONDS)
                        .subscribeOn(uiScheduler)
                        .observeOn(uiScheduler)
                        .doOnNext {
                            emitter.onNext(it)
                        }
                        .subscribe {}.apply {
                            compositeDisposable.addAll(this)
                        }
            }

    override fun scheduleIntervalWithCount(count: Long): Observable<Long> =
            Observable.create { emitter ->
                Observable.intervalRange(0L, count, 0L, 1000L, TimeUnit.MILLISECONDS)
                        .subscribeOn(uiScheduler)
                        .observeOn(uiScheduler)
                        .doOnNext {
                            emitter.onNext(it)
                        }
                        .subscribe {}.apply {
                            compositeDisposable.addAll(this)
                        }
            }

    override fun scheduleIntervalWithCountForColor(count: Long): Observable<Long> =
            Observable.create { emitter ->
                Observable.intervalRange(0L, count, 0L, 500L, TimeUnit.MILLISECONDS)
                        .subscribeOn(uiScheduler)
                        .observeOn(uiScheduler)
                        .doOnNext {
                            emitter.onNext(it)
                        }
                        .subscribe {}.apply {
                            compositeDisposable.addAll(this)
                        }
            }

    override fun disposeAll() {
        compositeDisposable.clear()
    }
}