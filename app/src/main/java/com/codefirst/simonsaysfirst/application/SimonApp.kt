package com.codefirst.simonsaysfirst.application

import android.support.multidex.MultiDexApplication
import com.codefirst.simonsaysfirst.basedi.DaggerMainComponent
import com.codefirst.simonsaysfirst.basedi.MainComponent
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class SimonApp : MultiDexApplication() {

    companion object {
        var mainComponent: MainComponent? = null
    }

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        mainComponent = DaggerMainComponent.builder()
                .bindContext(this)
                .build()
    }
}