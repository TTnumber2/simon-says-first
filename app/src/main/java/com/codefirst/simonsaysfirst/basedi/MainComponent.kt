package com.codefirst.simonsaysfirst.basedi

import android.content.Context
import com.codefirst.simonsaysfirst.application.SimonApp
import com.codefirst.simonsaysfirst.basedi.module.BaseRxModule
import com.codefirst.simonsaysfirst.basedi.module.NavigationModule
import com.codefirst.simonsaysfirst.basedi.module.SchedulersModule
import com.codefirst.simonsaysfirst.gameactivity.di.GameActivityComponent
import com.codefirst.simonsaysfirst.menuactivity.di.MenuActivityComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NavigationModule::class,
    SchedulersModule::class,
    BaseRxModule::class])
interface MainComponent {

    fun inject(cafApp: SimonApp)

    fun plusMenuActivityComponent(): MenuActivityComponent
    fun plusGameActivityBuilder(): GameActivityComponent.Builder

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindContext(context: Context): Builder

        fun build(): MainComponent
    }
}