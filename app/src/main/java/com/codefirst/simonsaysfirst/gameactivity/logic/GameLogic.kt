package com.codefirst.simonsaysfirst.gameactivity.logic

interface GameLogic {

    var steps: List<GameButton>
    var counter: Int

    fun attach(gameMode: GameMode)
    fun detach()
    fun pressedButton(gameButton: GameButton)
}