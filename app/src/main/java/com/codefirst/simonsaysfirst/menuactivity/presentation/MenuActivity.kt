package com.codefirst.simonsaysfirst.menuactivity.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.codefirst.simonsaysfirst.R
import com.codefirst.simonsaysfirst.application.SimonApp
import kotlinx.android.synthetic.main.choose_game_type_menu.*
import javax.inject.Inject

class MenuActivity : AppCompatActivity(), MenuActivityMVP.View {

    @Inject
    lateinit var presenter: MenuActivityMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        SimonApp.mainComponent!!.plusMenuActivityComponent().inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_game_type_menu)

        initViews()
    }

    private fun initViews() {
        game_type_infinite.setOnClickListener {
            presenter.openInfiniteMode()
        }
        game_type_standard.setOnClickListener {
            presenter.openStandardMode()
        }
    }
}