package com.codefirst.simonsaysfirst.gameactivity.logic

import io.reactivex.Observer
import java.util.Random
import javax.inject.Inject

class GameLogicImpl
@Inject constructor(private val gameInfoObserver: Observer<GameInformation>) : GameLogic {

    override var steps: List<GameButton> = mutableListOf()

    override var counter: Int = 0

    private var gameMode: GameMode? = null

    override fun attach(gameMode: GameMode) {
        this.gameMode = gameMode
        randomNextColour()
        gameInfoObserver.onNext(GameInformation.START)
    }

    override fun detach() {
        gameMode = null
        steps = mutableListOf()
        counter = 0
    }

    override fun pressedButton(gameButton: GameButton) {
        if (steps[counter] != gameButton) {
            gameInfoObserver.onNext(GameInformation.WRONG)
            return
        }
        counter += 1

        if (counter >= steps.count()) {
            randomNextColour()

            if (gameMode == GameMode.STANDARD_MODE && steps.count() == 33) {
                gameInfoObserver.onNext(GameInformation.WIN)
                return
            }

            gameInfoObserver.onNext(GameInformation.NEXT)
        }
    }

    private fun randomNextColour() {
        when (Random().nextInt(4)) {
            0 -> steps += GameButton.GREEN
            1 -> steps += GameButton.RED
            2 -> steps += GameButton.YELLOW
            3 -> steps += GameButton.BLUE
        }
    }
}