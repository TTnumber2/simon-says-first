package com.codefirst.simonsaysfirst.gameactivity.presentation

import com.codefirst.simonsaysfirst.gameactivity.logic.GameButton

interface GameActivityMVP {

    interface Presenter {

        fun attach(view: View)
        fun detach()
        fun pressedButton(gameButton: GameButton)
    }

    interface View {
        fun showCountDown(timer: Long)
        fun showSimonSaysDoIt()
        fun showSimonSaysWatch()
        fun turnOnButton(gameButton: GameButton)
        fun turnOffButton(gameButton: GameButton)
        fun turnOffClicksOnButton()
        fun turnOnClicksOnButton()
        fun showWrongBanner()
        fun goBackToMenu()
        fun showRoundNumber(round: Int)
        fun showBest(best: Int)
    }
}