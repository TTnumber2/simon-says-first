package com.codefirst.simonsaysfirst.menuactivity.di

import com.codefirst.simonsaysfirst.menuactivity.presentation.MenuActivity
import dagger.Subcomponent

@Subcomponent(modules = [MenuActivityModule::class])
interface MenuActivityComponent {

    fun inject(activity: MenuActivity)
}