package com.codefirst.simonsaysfirst.gameactivity.logic

enum class GameInformation {

    START,
    NEXT,
    WRONG,
    WIN
}