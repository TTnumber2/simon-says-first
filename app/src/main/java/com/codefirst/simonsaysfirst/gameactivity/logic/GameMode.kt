package com.codefirst.simonsaysfirst.gameactivity.logic

enum class GameMode {

    INFINITY_MODE,
    STANDARD_MODE
}