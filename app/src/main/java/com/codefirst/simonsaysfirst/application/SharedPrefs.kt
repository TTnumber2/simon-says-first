package com.codefirst.simonsaysfirst.application

interface SharedPrefs {

    fun saveBestScoreInfinityGameMode(best: Int)
    fun getBestScoreInfinityGameMode(): Int
}