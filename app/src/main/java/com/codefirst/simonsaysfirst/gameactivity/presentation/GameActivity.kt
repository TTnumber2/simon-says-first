package com.codefirst.simonsaysfirst.gameactivity.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import com.codefirst.simonsaysfirst.R
import com.codefirst.simonsaysfirst.application.SimonApp
import com.codefirst.simonsaysfirst.gameactivity.logic.GameButton
import com.codefirst.simonsaysfirst.gameactivity.logic.GameMode.INFINITY_MODE
import com.codefirst.simonsaysfirst.gameactivity.logic.GameMode.STANDARD_MODE
import com.codefirst.simonsaysfirst.gameactivity.logic.GameMode.valueOf
import kotlinx.android.synthetic.main.activity_game.*
import javax.inject.Inject

class GameActivity : AppCompatActivity(), GameActivityMVP.View {

    companion object {

        const val GAME_MODE = "game_mode"

        fun openGameWithInfiniteMode(context: Context) {
            context.startActivity(Intent(context, GameActivity::class.java).apply {
                this.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                this.putExtra(GAME_MODE, INFINITY_MODE.name)
            })
        }

        fun openGameWithStandardMode(context: Context) {
            context.startActivity(Intent(context, GameActivity::class.java).apply {
                this.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                this.putExtra(GAME_MODE, STANDARD_MODE.name)
            })
        }
    }

    @Inject
    lateinit var presenter: GameActivityMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        SimonApp.mainComponent!!.plusGameActivityBuilder()
                .buildGameMode(valueOf(intent.extras.getString(GAME_MODE)))
                .build()
                .inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        presenter.attach(this)
    }

    private fun changeAlphaOnClick() {
        simon_says_green_panel.setOnTouchListener(getGenericMotionListener(GameButton.GREEN))
        simon_says_red_panel.setOnTouchListener(getGenericMotionListener(GameButton.RED))
        simon_says_yellow_panel.setOnTouchListener(getGenericMotionListener(GameButton.YELLOW))
        simon_says_blue_panel.setOnTouchListener(getGenericMotionListener(GameButton.BLUE))
    }

    private fun dontChangeAlphaOnClick() {
        simon_says_green_panel.setOnTouchListener(null)
        simon_says_red_panel.setOnTouchListener(null)
        simon_says_yellow_panel.setOnTouchListener(null)
        simon_says_blue_panel.setOnTouchListener(null)
    }

    private fun getGenericMotionListener(gameButton: GameButton) = View.OnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> v.alpha = 1.0f
            MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP -> {
                v.alpha = 0.5f
                presenter.pressedButton(gameButton)
            }
        }
        true
    }

    override fun showCountDown(timer: Long) {
        when (timer) {
            0L -> game_type_banner.setImageResource(R.drawable.simon_says_ready_3)
            1L -> game_type_banner.setImageResource(R.drawable.simon_says_ready_2)
            2L -> game_type_banner.setImageResource(R.drawable.simon_says_ready_1)
        }
    }

    override fun showSimonSaysDoIt() {
        game_type_banner.setImageResource(R.drawable.simon_says_do_it)
    }

    override fun showSimonSaysWatch() {
        game_type_banner.setImageResource(R.drawable.simon_says_watch)
    }

    override fun turnOnButton(gameButton: GameButton) {
        when (gameButton) {
            GameButton.GREEN -> simon_says_green_panel.alpha = 1f
            GameButton.RED -> simon_says_red_panel.alpha = 1f
            GameButton.YELLOW -> simon_says_yellow_panel.alpha = 1f
            GameButton.BLUE -> simon_says_blue_panel.alpha = 1f
        }
    }

    override fun turnOffButton(gameButton: GameButton) {
        when (gameButton) {
            GameButton.GREEN -> simon_says_green_panel.alpha = 0.5f
            GameButton.RED -> simon_says_red_panel.alpha = 0.5f
            GameButton.YELLOW -> simon_says_yellow_panel.alpha = 0.5f
            GameButton.BLUE -> simon_says_blue_panel.alpha = 0.5f
        }
    }

    override fun turnOffClicksOnButton() {
        dontChangeAlphaOnClick()
    }

    override fun turnOnClicksOnButton() {
        changeAlphaOnClick()
    }

    override fun showWrongBanner() {
        game_type_banner.setImageResource(R.drawable.simon_says_wrong_one)
    }

    override fun goBackToMenu() {
        onBackPressed()
    }

    override fun showRoundNumber(round: Int) {
        game_round.visibility = View.VISIBLE
        game_round.text = "Round: $round"
    }

    override fun showBest(best: Int) {
        game_round.visibility = View.VISIBLE
        game_round.text = "Best score: $best"
    }

    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }
}