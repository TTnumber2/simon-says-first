package com.codefirst.simonsaysfirst.rxextension

import io.reactivex.Observable

interface RxExtensionScheduler {

    fun scheduleInterval(): Observable<Long>
    fun scheduleIntervalWithCount(count: Long): Observable<Long>
    fun scheduleIntervalWithCountForColor(count: Long): Observable<Long>
    fun disposeAll()
}