package com.codefirst.simonsaysfirst.gameactivity.presentation

import android.annotation.SuppressLint
import com.codefirst.simonsaysfirst.application.SharedPrefs
import com.codefirst.simonsaysfirst.basedi.module.SchedulersModule.SchedulerIO
import com.codefirst.simonsaysfirst.basedi.module.SchedulersModule.SchedulerUI
import com.codefirst.simonsaysfirst.gameactivity.logic.GameButton
import com.codefirst.simonsaysfirst.gameactivity.logic.GameInformation
import com.codefirst.simonsaysfirst.gameactivity.logic.GameLogic
import com.codefirst.simonsaysfirst.gameactivity.logic.GameMode
import com.codefirst.simonsaysfirst.rxextension.RxExtensionScheduler
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposables
import javax.inject.Inject

@SuppressLint("CheckResult")
class GameActivityPresenter
@Inject constructor(private val sharedPrefs: SharedPrefs,
                    private val gameMode: GameMode,
                    private val rxScheduler: RxExtensionScheduler,
                    private val gameLogic: GameLogic,
                    private val gameInfoObservable: Observable<GameInformation>,
                    @SchedulerIO private val ioScheduler: Scheduler,
                    @SchedulerUI private val uiScheduler: Scheduler) : GameActivityMVP.Presenter {

    private var view: GameActivityMVP.View? = null
    private val compositeDisposable = CompositeDisposable()
    private var gameLogicDisposable = Disposables.disposed()

    override fun attach(view: GameActivityMVP.View) {
        this.view = view
        subscribeToGameInfo()

        gameLogic.attach(gameMode)
    }

    override fun pressedButton(gameButton: GameButton) {
        gameLogic.pressedButton(gameButton)
    }

    private fun subscribeToGameInfo() {
        gameInfoObservable.observeOn(uiScheduler)
                .map {
                    if (gameMode == GameMode.STANDARD_MODE) {
                        view?.showRoundNumber(gameLogic.steps.count())
                    } else if (gameMode == GameMode.INFINITY_MODE) {
                        gameLogic.steps.count().apply {
                            if (sharedPrefs.getBestScoreInfinityGameMode() < this) {
                                sharedPrefs.saveBestScoreInfinityGameMode(this - 1)
                            }
                            view?.showBest(sharedPrefs.getBestScoreInfinityGameMode())
                        }
                    }
                    it
                }
                .observeOn(uiScheduler)
                .subscribeOn(uiScheduler)
                .subscribe {
                    when (it) {
                        GameInformation.NEXT -> {
                            startShowingColours()
                        }
                        GameInformation.WRONG -> {
                            restartGameLogic()
                            view?.turnOffClicksOnButton()
                            view?.showWrongBanner()
                            countDownTillActivityClose()
                        }
                        GameInformation.START -> {
                            startShowingColours()
                        }
                        GameInformation.WIN -> {

                        }
                    }
                }.apply {
                    gameLogicDisposable = this
                }
    }

    private fun countDownTillActivityClose() {
        rxScheduler.scheduleIntervalWithCount(4)
                .observeOn(uiScheduler)
                .subscribeOn(uiScheduler)
                .subscribe {
                    if (it == 3L) {
                        view?.goBackToMenu()
                    }
                }.apply {
                    compositeDisposable.add(this)
                }
    }

    private fun startShowingColours() {
        restartGameLogic()
        view?.turnOffClicksOnButton()
        countDownForColoursRepeat()
    }

    private fun restartGameLogic() {
        gameLogic.counter = 0
        rxScheduler.disposeAll()
        compositeDisposable.clear()
    }

    private fun countDownForColoursRepeat() {
        rxScheduler.scheduleIntervalWithCount(4)
                .observeOn(uiScheduler)
                .subscribeOn(uiScheduler)
                .subscribe {
                    view?.showCountDown(it)
                    if (it == 3L) {
                        view?.showSimonSaysWatch()
                        repeatSimonSaysColours()
                    }
                }.apply {
                    compositeDisposable.add(this)
                }
    }

    private fun repeatSimonSaysColours() {
        var counter = 0
        rxScheduler.scheduleIntervalWithCountForColor(gameLogic.steps.count().toLong() * 2)
                .observeOn(uiScheduler)
                .subscribeOn(uiScheduler)
                .subscribe {
                    blinkCertainColor(it.toInt(), gameLogic.steps[counter], counter == gameLogic.steps.count() - 1)

                    if (it.toInt() % 2 == 1) {
                        counter += 1
                    }
                }.apply {
                    compositeDisposable.add(this)
                }
    }

    private fun blinkCertainColor(time: Int, gameButton: GameButton, isLast: Boolean) {
        view?.turnOffButton(gameButton)

        if (time % 2 == 1) {
            view?.turnOffButton(gameButton)

            if (isLast) {
                view?.turnOnClicksOnButton()
                view?.showSimonSaysDoIt()
            }
        } else {
            view?.turnOnButton(gameButton)
        }
    }

    override fun detach() {
        gameLogicDisposable.dispose()
        compositeDisposable.clear()
        gameLogic.detach()
        view = null
    }
}