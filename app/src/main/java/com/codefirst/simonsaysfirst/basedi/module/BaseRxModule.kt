package com.codefirst.simonsaysfirst.basedi.module

import com.codefirst.simonsaysfirst.rxextension.RxExtensionScheduler
import com.codefirst.simonsaysfirst.rxextension.RxExtensionSchedulerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class BaseRxModule {

    @Binds
    abstract fun bindRxExtensionScheduler(scheduler: RxExtensionSchedulerImpl): RxExtensionScheduler
}